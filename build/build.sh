#!/bin/bash

set -ue
cd "$(dirname "$0")"

download-until-correct () {
  local url="$1"
  local filename="$2"
  local sha1="$3"

  until sha1sum -c <<<"$sha1  $filename" ; do
    rm -rvf "$filename"
    wget --continue --no-check-cert "$url" -O "$filename" || true
    sleep 1
  done
}

download-until-correct                                                               \
  ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.39.tar.bz2                 \
  pcre-8.39.tar.bz2                                                                  \
  5e38289fd1b4ef3e8426f31a01e34b6924d80b90

download-until-correct                                                               \
  http://nginx.org/download/nginx-1.10.1.tar.gz                                           \
  nginx-1.10.1.tar.gz                                                                     \
  9c5d4e06d309bbe2efa41f09dd53912e3c3d3a75

download-until-correct                                                               \
  https://github.com/cuber/ngx_http_google_filter_module/archive/0.2.0.tar.gz             \
  ngx_http_google_filter_module-0.2.0.tgz                                                 \
  7e9c68a9ed765d90dfc52716bdf902bb3b80fcbe

download-until-correct                                                               \
  https://github.com/yaoweibin/ngx_http_substitutions_filter_module/archive/v0.6.4.tar.gz \
  ngx_http_substitutions_filter_module-0.6.4.tgz                                          \
  4b586a95411216fe64811758f05975ccf450a490

tar xf pcre-8.39.tar.bz2
tar xf nginx-1.10.1.tar.gz
tar xf ngx_http_substitutions_filter_module-0.6.4.tgz
tar xf ngx_http_google_filter_module-0.2.0.tgz

pushd nginx-1.10.1
./configure --prefix="$PWD/../../nginx"                      \
  --add-module=../ngx_http_google_filter_module-0.2.0        \
  --add-module=../ngx_http_substitutions_filter_module-0.6.4 \
  --with-http_ssl_module --with-ipv6 --with-pcre=../pcre-8.39

make install
