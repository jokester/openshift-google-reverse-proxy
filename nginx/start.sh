#!/bin/bash
set -ue
cd "$(dirname "$0")"

cat < nginx.conf.template                                                                       \
  | sed "s/OPENSHIFT_DIY_IP/$OPENSHIFT_DIY_IP/g"                                                \
  | sed "s/OPENSHIFT_RESOLVER/$(grep nameserver /etc/resolv.conf  | head -1 | cut -d' ' -f2)/g" \
  > conf/nginx.conf

sbin/nginx -s quit && true

exec sbin/nginx
